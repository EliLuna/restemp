package com.elizabethluna.rest;

import com.elizabethluna.rest.empleados.Capacitacion;
import com.elizabethluna.rest.empleados.Empleado;
import com.elizabethluna.rest.empleados.Empleados;
import com.elizabethluna.rest.repositorios.EmpleadoDAO;
import com.elizabethluna.rest.utils.BadSeparator;
import com.elizabethluna.rest.utils.Configuracion;
import com.elizabethluna.rest.utils.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.websocket.server.PathParam;
import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path= "/empleados")
public class EmpleadosController {

    @Autowired
    EmpleadoDAO empleadoDAO;

    @GetMapping(path="/")
    public Empleados getEmpleados(){
        return empleadoDAO.getAllEmpleados();
    }

    @GetMapping(path="/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable int id){
        Empleado emp = empleadoDAO.getEmpleado(id);
        if(emp==null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok().body(emp);
        }
        //return empleadoDAO.getEmpleado(id);
    }

    @PostMapping("/")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp){
        Integer id= empleadoDAO.getAllEmpleados().getListaEmpleados().size() +1 ;
        emp.setId(id);
        empleadoDAO.addEmpleado(emp);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/id")
                .buildAndExpand(emp.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping(value = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@RequestBody Empleado emp){
        empleadoDAO.updEmpleado(emp);
        return  ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> delEmpleadoXId(@PathVariable int id){
        String resp = empleadoDAO.deleteEmpleado(id);
        if (resp == "Ok"){
            return  ResponseEntity.ok().build();
        }else{
            return  ResponseEntity.noContent().build();
        }
    }

    @PatchMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> softUpdateEmpleado(@PathVariable int id, @RequestBody Map<String,Object> updates){
        empleadoDAO.updateSoftEmpleado(id,updates);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/{id}/capacitaciones")
    public ResponseEntity<List<Capacitacion>> getCapacitacionesEmpleado(@PathVariable int id){
        return ResponseEntity.ok().body(empleadoDAO.getCapacitacionesEmpleados(id));
    }

    @PostMapping(path = "/{id}/capacitaciones", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@PathVariable int id,@RequestBody Capacitacion cap){
        if(empleadoDAO.addCapacitacion(id,cap)){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @Value("${app.titulo}")private String titulo;
    @Value("${app.autor}")private String autor;

    @GetMapping("/autor")
    public String getAppAutor(){
        return autor;
    }

    @GetMapping("/titulo")
    public String getAppTitulo(){
        return titulo;
    }



    @Autowired
    private Environment env;
    @Autowired
    Configuracion configuracion;

    @GetMapping("/modo")
    public String getModo(){
        String modo = configuracion.getModo();
        return String.format("%S (%S)", titulo, modo);
    }

    @GetMapping("/cadena")
    public String getCadena(@RequestParam String texto,@RequestParam String separador){
        try {
            return Utilidades.getCadena(texto, separador);

        }catch(Exception ex){
            return "";
        }
    }

}
