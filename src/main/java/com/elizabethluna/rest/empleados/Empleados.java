package com.elizabethluna.rest.empleados;

import java.util.ArrayList;
import java.util.List;

public class Empleados {

    private List<Empleado> listEmpleados;

    public List<Empleado> getListaEmpleados(){
        if(listEmpleados==null){
            listEmpleados = new ArrayList<>();
        }
        return listEmpleados;
    }

    public void setListEmpleados(List<Empleado> listEmpleados) {
        this.listEmpleados = listEmpleados;
    }
}
