package com.elizabethluna.rest;

import com.elizabethluna.rest.EmpleadosController;
import com.elizabethluna.rest.utils.BadSeparator;
import com.elizabethluna.rest.utils.EstadosPedido;
import com.elizabethluna.rest.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@EnableConfigurationProperties //acceder a propiedades y las utliza
@SpringBootTest
public class EmpleadosControllerTest {

    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testGetCadena() throws BadSeparator {
        String correcto = "L.U.Z D.E.L S.O.L";
        String origen= "luz del sol";
        assertEquals(correcto,empleadosController.getCadena(origen,"."));
    }

    @Test
    public void testSeparadorGetCadena(){
        try{
            Utilidades.getCadena("Elizabeth Luna","..");
            fail("Se esperaba BadSeparator");
        }catch(BadSeparator bs){

        }
    }

    @Test
    public void testGetAutor(){
        assertEquals("\"Elizabeth Luna\"",empleadosController.getAppAutor());
    }

    @ParameterizedTest
    @ValueSource(ints = {1,3,4,5,16,-13,Integer.MAX_VALUE})
    public void testEsImpar(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings = {""," "})
    public void testEstaBlanco(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"","\t","\n"})
    public void testEstaBlancoCompleto(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstadosPedido(EstadosPedido ep){
        assertTrue(Utilidades.valorarEstadoPedido(ep));
    }
}
